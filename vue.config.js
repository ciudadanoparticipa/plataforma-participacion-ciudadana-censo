const GATEWAY_PROXY_BASE_URL = process.env.VUE_APP_GATEWAY_BASE_URL
const REDMINE_PROXY_BASE_URL = process.env.VUE_APP_REDMINE_BASE_URL

module.exports = {
  devServer: {
    proxy: {
      '/redmineapi': {
        target: REDMINE_PROXY_BASE_URL,
        ws: true,
        // changeOrigin: true,
        logLevel: 'debug',
        pathRewrite: {
          '/redmineapi': ''
        }
      },
      '/gateway': {
        target: GATEWAY_PROXY_BASE_URL,
        ws: true,
        // changeOrigin: true,
        logLevel: 'debug',
        pathRewrite: {
          '/gateway': ''
        }
      }
    },
    headers: {
      'X-Frame-Options': `ALLOW-FROM ${REDMINE_PROXY_BASE_URL}`
    }
  },
  transpileDependencies: [
    /\/node_modules\/vue-echarts\//,
    /\/node_modules\/resize-detector\//
  ]
}
