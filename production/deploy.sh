#!/bin/bash

echo "Login in docker with gitlab registry"
echo "Ingresa las credenciales de gitlab (Creada para Jenkins o personal)"
docker login registry.gitlab.com

# Cambiar unicamente esta variable
BACKUP_SERVER_IP=51.15.15.75

BACKUP_DB_NAME=initial.sql
BACKUP_ZIP_NAME=initial.zip
BACKUP_DB_URL=${BACKUP_SERVER_IP}:12004/db/$BACKUP_ZIP_NAME
DATABASE_NAME=redmine
DOCKER_DB_CONTAINER_NAME=production_db_1

echo -e "Deploying...\n"

echo "docker pull images"
docker-compose pull

echo "Start database service"
docker-compose up -d db

echo "Start restor db"
echo "curl -LO $BACKUP_DB_URL"
curl -LO $BACKUP_DB_URL

echo -e "unzip $BACKUP_ZIP_NAME\n"
unzip $BACKUP_ZIP_NAME
rm $BACKUP_ZIP_NAME

echo -e "OBS: En caso de error ejecutar los pasos mencionados abajo:\n"

echo "Paso 1: Verifique que la base de datos se está ejecutando."
docker exec -i $DOCKER_DB_CONTAINER_NAME ps -ef | grep postgres

echo -e "\nPaso 2: Utilice find para buscar la ubicación del socket, que debería estar en algún lugar de /tmp"
echo "find /tmp/ -name .s.PGSQL.5432"
docker exec -i $DOCKER_DB_CONTAINER_NAME find /tmp/ -name .s.PGSQL.5432

echo -e "\nPaso 3: Si no puede encontrar el socket, pero observa que el servicio se está ejecutando, verifique que el archivo pg_hba.conf permita sockets locales."

echo -e "\ncat /var/lib/postgresql/data/pg_hba.conf \n"

docker exec -i $DOCKER_DB_CONTAINER_NAME cat /var/lib/postgresql/data/pg_hba.conf

echo -e "\nDe forma predeterminada, cerca de la parte inferior del archivo debería ver las siguientes líneas:\n"

echo -e "# TYPE  DATABASE        USER            ADDRESS                 METHOD\n"
echo '# "local" is for Unix domain socket connections only'
echo -e "local    \t    all      \t\s\s\s      all         \t        trust\n"

echo -e "Si no lo ve, puede modificar el archivo y reiniciar el servicio de postgres.\n"

cat > $CREATE_REDMINE_ROLE <<- EOM
CREATE ROLE redmine
  LOGIN
  ENCRYPTED PASSWORD 'md5c5a58b543b1cad88fa10f629c0e535c1'
  SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;
EOM

cat > $CREATE_REDMINE_DB <<- EOM
CREATE DATABASE redmine
  WITH OWNER = redmine
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    CONNECTION LIMIT = -1;
EOM

echo -e "\nsleep 5"
sleep 5

echo "Create redmine role"

echo -e "\n$CREATE_REDMINE_ROLE\n"

echo "$CREATE_REDMINE_ROLE\gexec" | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME

echo "Create redmine database"

echo -e "\n$CREATE_REDMINE_DB\n"

echo "$CREATE_REDMINE_DB\gexec" | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME

echo "Restoring..."
echo "cat $BACKUP_DB_NAME cat | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME"
cat $BACKUP_DB_NAME cat | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME
rm $BACKUP_DB_NAME

echo "Finished restore db"

docker-compose up -d
