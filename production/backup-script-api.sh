#!/bin/bash
set -e
set -o xtrace
echo "Start Backup"
date

# Cambiar unicamemnte en caso de configurar el docker compose de otra forma
BACKUP_DIR=./volumes/backups
DOCKER_API_CONTAINER_NAME=production_api_1
DOCKER_API_STATIC_DIR=/usr/src/app/public/
NUMBER_OF_DAYS=180

# Fecha del nombre del backup
BACKUP_DATE=`date +%Y%m%d%H%M%S`

BACKUP_API_DIR=$BACKUP_DIR/api/

# Nombre con el cual va a guardar el backup final
FULL_NAME=$BACKUP_API_DIR/$BACKUP_DATE\.zip
LATEST_NAME=$BACKUP_API_DIR/latest\.zip

echo "$FULL_NAME"

mkdir -p api_static_folder
echo "[DOCKER] copy api static folder (container) into ./api_static_folder (server)"
docker cp $DOCKER_API_CONTAINER_NAME:$DOCKER_API_STATIC_DIR/. ./api_static_folder

mkdir -p $BACKUP_API_DIR

echo "[SERVER] zip -r $FULL_NAME ./api_static_folder"
zip -r $FULL_NAME ./api_static_folder
rm -r ./api_static_folder

# Copy last backup with latest file name
echo "cp $FULL_NAME $LATEST_NAME"
if [ -f "$LATEST_NAME" ]; then rm $LATEST_NAME; fi
cp $FULL_NAME $LATEST_NAME

# Remove backup from longer $NUMBER_OF_DAYS days ago
echo "Removing backup from longer $NUMBER_OF_DAYS days ago"
find $BACKUP_API_DIR -type f -prune -mtime +$NUMBER_OF_DAYS -exec rm -f {} \;

echo "Finished"
