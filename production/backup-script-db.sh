#!/bin/bash
set -e
set -o xtrace
echo "Start Backup"
date

# Cambiar unicamemnte en caso de configurar el docker compose de otra forma
BACKUP_DIR=./volumes/backups
DATABASE_NAME=redmine
DOCKER_DB_CONTAINER_NAME=production_db_1
NUMBER_OF_DAYS=180

#String to append to the name of the backup files
BACKUP_DATE=`date +%Y%m%d%H%M%S`

BACKUP_DB_DIR=$BACKUP_DIR/db/

BACKUP_NAME=backup.sql

# nombre con el cual va a guardar el backup final
FULL_NAME=$BACKUP_DB_DIR$DATABASE_NAME\_$BACKUP_DATE\.zip
LATEST_NAME=$BACKUP_DB_DIR/latest\.zip

echo "$FULL_NAME"
mkdir -p $BACKUP_DB_DIR

echo "[DOCKER] Dumping $DATABASE_NAME"
docker exec $DOCKER_DB_CONTAINER_NAME pg_dump -h localhost -U redmine $DATABASE_NAME -f /tmp/$BACKUP_NAME

echo "[SERVER] Copy to $DATABASE_NAME\_$BACKUP_DATE\.sql"
docker cp $DOCKER_DB_CONTAINER_NAME:/tmp/$BACKUP_NAME .

echo "zip -j $FULL_NAME $BACKUP_NAME"
zip -j $FULL_NAME $BACKUP_NAME
rm $BACKUP_NAME

# Copy last backup with latest file name
echo "cp $FULL_NAME $LATEST_NAME"
if [ -f "$LATEST_NAME" ]; then rm $LATEST_NAME; fi
cp $FULL_NAME $LATEST_NAME

# Remove backup from longer $NUMBER_OF_DAYS days ago
echo "Removing backup from longer $NUMBER_OF_DAYS days ago"
find $BACKUP_DB_DIR -type f -prune -mtime +$NUMBER_OF_DAYS -exec rm -f {} \;

echo "Finished"
