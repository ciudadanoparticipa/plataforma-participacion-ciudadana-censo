import Vue from 'vue'
import L from 'leaflet'
import 'leaflet.smooth_marker_bouncing'
import 'leaflet/dist/leaflet.css'

import { LControlZoom, LControlLayers, LMap, LMarker, LTileLayer, LWMSTileLayer, LPopup, LTooltip, LGeoJson } from 'vue2-leaflet'

Vue.component(LMap.name, LMap)
Vue.component(LControlZoom.name, LControlZoom)
Vue.component(LControlLayers.name, LControlLayers)
Vue.component(LMarker.name, LMarker)
Vue.component(LTileLayer.name, LTileLayer)
Vue.component(LWMSTileLayer.name, LWMSTileLayer)
Vue.component(LPopup.name, LPopup)
Vue.component(LTooltip.name, LTooltip)
Vue.component(LGeoJson.name, LGeoJson)

delete L.Icon.Default.prototype._getIconUrl

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})
