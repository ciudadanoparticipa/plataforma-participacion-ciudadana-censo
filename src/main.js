import 'babel-polyfill'

import '@/styles/material-colors/custom.scss'

import Vue from 'vue'
import App from './App.vue'

import { sync } from 'vuex-router-sync'
import { addListenerForResizeRoot, hasWidthGt1920, isMobile, isLandscape } from './utils/responsive'
import VueProgressiveImage from 'vue-progressive-image'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueAnalytics from 'vue-analytics'
import Vuelidate from 'vuelidate'
import store from './store'
import runPoly from './fixie'
import router from './router'
import loadData from './data-loader'
import watchers from './store-watchers'

import '@/components'
import './vue-leaflet'
import './material'
import './resource'
import './charts'
import './echarts'
import './vuetify'

runPoly()

addListenerForResizeRoot()

Vue.use(VueProgressiveImage)
Vue.use(VueAwesomeSwiper)
Vue.use(Vuelidate)

Vue.config.productionTip = false

window.isMobile = isMobile()
window.isLandscape = isLandscape()
window.widthGt1920 = hasWidthGt1920()

sync(store, router)

router.beforeEach((to, from, next) => {
  if (store.getters.shouldGoToLogin(to)) return next('login')
  next()
})

Vue.use(VueAnalytics, {
  id: 'UA-124694098-1',
  router
})

const vueInstance = new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

loadData(vueInstance)
watchers(vueInstance)
