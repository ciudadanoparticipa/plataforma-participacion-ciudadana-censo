import { defaultMessageError } from '@/constants.js'

export default {
  data: () => ({
    errorDialog: false,
    errorDialogMessage: `Hubo un error. ${defaultMessageError}`,
    errorDialogConfirm: () => null
  }),
  methods: {
    showErrorMessage (value, callback = () => null) {
      this.errorDialogConfirm = () => { this.errorDialog = false }
      if (typeof value === 'object') {
        const error = value.err || value
        const { defaultMessage, body } = error
        this.errorDialogMessage = (body && body.error && body.error.message) ? body.error.message : defaultMessage
        if (/ECONNREFUSED/.test(this.errorDialogMessage)) this.errorDialogMessage = 'El Redmine no responde o está caído'
        console.error(value, defaultMessage)
      } else this.errorDialogMessage = value
      this.errorDialog = true
      callback()
    }
  }
}
