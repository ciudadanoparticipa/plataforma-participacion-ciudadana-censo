import formWithValidationMixin from '@/mixins/form-with.validation'

export default {
  mixins: [ formWithValidationMixin ],
  props: {
    formData: {
      type: [Object, Array],
      default: () => ({})
    }
  },
  mounted () {
    this.loadForm()
  },
  methods: {
    submit (event, tab) {
      event.preventDefault()
      this.$emit('on-valid', {
        tab,
        isValid: this.isValid(),
        data: this.form
      })
    },
    hasData (obj) { return obj && Object.values(obj).length },
    loadForm () { }
  },
  watch: {
    formData () {
      this.loadForm()
    }
  }
}
