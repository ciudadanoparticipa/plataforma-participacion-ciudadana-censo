import formWithValidationMixin from '@/mixins/form-with.validation'

export default {
  mixins: [ formWithValidationMixin ],
  props: {
    value: {
      type: [ Object, Array ],
      default: () => null
    }
  },
  watch: {
    value: {
      handler (newVal, oldVal) {
        if (!oldVal) this.$emit('input', newVal)
      },
      deep: true
    }
  }
}
