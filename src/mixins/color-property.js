export default {
  props: {
    color: {
      type: String,
      default: '#ef7926'
    }
  },
  methods: {
    getColorPropertyStyle (project, isMap = false) {
      return { color: project.markerType === 'planeacion' && !isMap ? 'white' : project.color }
    },
    getBackgroundColorPropertyStyle (color) {
      return { 'background-color': color }
    },
    getScopeName () {
      return this.$refs.component ? Object.values(this.$refs.component.attributes).map(a => a.name).find(a => a.startsWith('data-v')) : ''
    },
    removeHashtag (str) {
      if (!str) return str
      return str.replace('#', '')
    }
  }
}
