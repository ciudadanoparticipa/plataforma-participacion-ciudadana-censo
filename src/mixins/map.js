import L from 'leaflet'

export default {
  mounted () {
    this.isMounted = true
    this.setInterfacesControl()
    this.setGlobalBounds()
  },
  data: () => ({
    isMounted: false,
    sidebar: null,
    center: L.latLng(-23.58071371351829, -58.48046875000001), // Center in Paraguay
    zoom: 6,
    layers: [
      {
        name: 'Mapa de Calles',
        visible: true,
        url: 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png',
        attribution:
          '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      },
      {
        name: 'Mapa Satelital',
        visible: false,
        url:
          'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        attribution:
          'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Communit'
      }
    ],
    globalBounds: null
  }),
  methods: {
    setInterfacesControl () {},

    getAllLatLngInMap () {
      const arrayOfLatLngs = []
      this.map.eachLayer(function (layer) {
        if (layer.getLatLng || layer.getBounds) {
          if (layer.getBounds) {
            const bounds = layer.getBounds()
            arrayOfLatLngs.push(bounds._northEast)
            arrayOfLatLngs.push(bounds._southWest)
          } else {
            arrayOfLatLngs.push(layer.getLatLng())
          }
        }
      })
      return arrayOfLatLngs
    },

    getAllBounds () {
      return new L.LatLngBounds(this.getAllLatLngInMap())
    },

    setGlobalBounds () {
      setTimeout(() => {
        this.globalBounds = this.getAllBounds()
      })
    }
  },
  computed: {
    map () {
      if (!this.isMounted) return
      return this.$refs.map.mapObject
    }
  }
}
