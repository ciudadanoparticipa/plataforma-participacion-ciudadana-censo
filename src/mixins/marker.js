import L from 'leaflet'
import { getData } from '@/utils/object.js'
import mapChildMixin from './map-child'

const activeClassName = 'active'

export default {
  mixins: [ mapChildMixin ],
  data: () => ({
    lastMarker: {},
    markerDataIdProperty: 'id',
    markers: []
  }),
  methods: {
    reflectNewRoute () {
      const self = this
      self.getDefaultIcon(self.lastMarker)
      if (!self.lmapObject) return
      if (!self.markerData) return
      setTimeout(_ => {
        const marker = self.getMarkerObject()
        if (!marker) return
        self.setMarkerInCenterAndChangeStyle(marker)
      })
    },
    getMarkerObject () {
      const self = this
      if (!self.markerData[self.markerDataIdProperty]) return
      return self.markers.find(marker => marker[self.markerDataIdProperty] === self.markerData[self.markerDataIdProperty])
    },
    setMarkerInCenterAndChangeStyle (marker) {
      const self = this
      setTimeout(_ => {
        self.centerMarker(marker)
      })
    },
    setLastMarker (marker) {
      if (getData(getData(this.lastMarker.feature).properties)[this.markerDataIdProperty] === getData(this.markerData)[this.markerDataIdProperty]) return
      this.lastMarker = marker
    },
    getSelectedIcon (marker) {
      if (!marker) return
      if (!marker.getElement) return
      const $el = marker.getElement()
      $el.classList.add(this.getClassIfActive(this.markerData))
    },
    getDefaultIcon (marker) {
      if (!(marker && marker.getElement)) return
      const $marker = marker.getElement()
      if (!($marker && $marker.classList)) return
      $marker.classList.remove(activeClassName)
    },
    isActive (item) {
      return this.markerDataId === item[this.markerDataIdProperty]
    },
    getClassIfActive (item) {
      return this.isActive(item) ? activeClassName : ''
    },
    centerMarker (marker) {
      this.getSelectedIcon(marker)
      this.lmapObject.fitBounds(L.latLngBounds([marker.getLatLng()]))
      this.setLastMarker(marker)
    },
    setMarkerObject (markerObject, identifier) {
      markerObject.identifier = identifier
      markerObject[this.markerDataIdProperty] = identifier
      for (let i = 0, len = this.markers.length; i < len; i++) {
        if (this.markers[i].identifier === markerObject.identifier) {
          this.markers[i] = markerObject
          return
        }
      }
      this.markers.push(markerObject)
    }
  },
  computed: {
    markerDataId () {
      return getData(this.markerData)[this.markerDataIdProperty]
    }
  },
  watch: {
    '$route': {
      handler () {
        this.reflectNewRoute()
      },
      immediate: true
    },
    markerData: {
      handler () {
        this.reflectNewRoute()
      },
      immediate: true
    }
  }
}
