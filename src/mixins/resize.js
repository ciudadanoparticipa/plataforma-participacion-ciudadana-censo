import { getData } from '@/utils/object'
import { isMobile, hasWidthGt1920, isLandscape } from '@/utils/responsive'

export default {
  mounted () {
    this.resizeOnlyInMixin()
    window.addEventListener('resize', this.resizeOnlyInMixin)
  },
  data: () => ({
    isMobile: false,
    hasWidthGt1920: false,
    isLandscape: false
  }),
  methods: {
    resizeOnlyInMixin () {
      this.isMobile = !this.routeMetaHideMenuMobile() && isMobile()
      this.hasWidthGt1920 = hasWidthGt1920()
      this.isLandscape = isLandscape()
    },
    routeMetaHideMenuMobile () {
      return getData(this.routeMeta).hideMenuMobile
    }
  },
  computed: {
    routeMeta () {
      return getData(this.$store.state.route).meta
    }
  }
}
