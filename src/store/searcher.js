export default {
  state: {
    searchText: '',
    showSearchList: false
  },

  mutations: {
    setSearchText: (state, searchText) => { state.searchText = searchText },
    setShowSearchList: (state, showSearchList) => { state.showSearchList = showSearchList }
  },

  actions: {
    setSearchText: ({ commit }, searchText) => {
      commit('setSearchText', searchText)
    },
    setShowSearchList: ({ commit }, showSearchList) => {
      commit('setShowSearchList', showSearchList)
    }
  },

  getters: {
    getSearchText: state => state.searchText,
    getShowSearchList: state => state.showSearchList
  }
}
