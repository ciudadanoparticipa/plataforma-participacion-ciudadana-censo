import router from '@/router'

export default {
  getters: {
    isCurrentRoute: () => route => {
      let r = route
      if (typeof route === 'object') r = router.resolve(route).href
      return router.currentRoute.fullPath === r
    },
    isMapView: () => router.currentRoute.name === 'map',
    isGestionInundacionesView: () => router.currentRoute.query && router.currentRoute.query.ver,
    isDetailView: () => router.currentRoute.query.code || router.currentRoute.query.refugio_nro || router.currentRoute.query.institucion_id || router.currentRoute.query.centro_municipal_id,
    isRedmineMap: () => route => route.matched.some(record => record.meta.isRedmineMap),
    isRequiresAuth: () => route => route.matched.some(record => record.meta.requiresAuth),
    getCurrentRouteObj: () => ({
      fullPath: router.currentRoute.fullPath,
      hash: router.currentRoute.hash,
      meta: router.currentRoute.meta,
      name: router.currentRoute.name,
      params: router.currentRoute.params,
      path: router.currentRoute.path,
      query: router.currentRoute.query
    }),
    hrefify: () => routeObj => router.resolve(routeObj).href,
    isSolucionACortoPlazo: (_, getters) => /ver=refugios/.test(getters.hrefify(getters.getCurrentRouteObj)),
    isSolucionDefinitiva: (_, getters) => getters.isMapView && !getters.isSolucionACortoPlazo,
    isInProjectsView: (_, getters) => getters.isCurrentRoute({ name: 'projects' })
  }
}
