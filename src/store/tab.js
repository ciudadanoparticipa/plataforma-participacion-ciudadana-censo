import router from '@/router'
import { clone } from '@/utils/object'

const initialState = localStorage.tab ? JSON.parse(localStorage.getItem('tab')) : { tab: 'descripcion' }

const updateTabInLocalStorage = state => {
  if (!state && localStorage.getItem('tab') !== state) return
  localStorage.setItem('tab', JSON.stringify(state))
}

export default {
  state: {
    tab: clone(initialState)
  },

  mutations: {
    activeTab: (state, tab) => { state.tab = tab }
  },

  actions: {
    setActiveTab: ({ commit }, tab) => {
      commit('activeTab', tab)
      updateTabInLocalStorage(tab)
    },

    setActivateTab: ({ dispatch }, tab) => {
      dispatch('setActiveTab', tab)
      dispatch('activeTableAndUpdateRoute')
    },

    activeTableAndUpdateRoute: ({ getters }) => {
      if (getters.isMapView) {
        if (getters.isGestionInundacionesView) {
          if (getters.getProjectInfo) router.replace(`/map?ver=refugios&code=${getters.getProjectInfo.code}&tab=${getters.getActiveTab}`)
          else if (!getters.isDetailView) router.replace(`/map?ver=refugios`)
        } else {
          if (getters.getProjectInfo) router.replace(`/map?code=${getters.getProjectInfo.code}&tab=${getters.getActiveTab}`)
          else if (!getters.isDetailView) router.replace(`/map`)
        }
      } else {
        if (getters.getProjectInfo) router.replace(`/map?code=${getters.getProjectInfo.code}&tab=${getters.getActiveTab}`)
      }
    }
  },

  getters: {
    getActiveTab: state => state.tab
  }
}
