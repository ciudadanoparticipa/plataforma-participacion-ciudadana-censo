import router from '@/router'
import { clone, hasValue } from '@/utils/object'

const initialState = localStorage.session ? JSON.parse(localStorage.getItem('session')) : {
  user: null,
  message: ''
}

const updateSessionInLocalStorage = state => localStorage.setItem('session', JSON.stringify(state))

export default {
  state: clone(initialState),
  mutations: {
    setUser: (state, user) => {
      state.user = user
    },
    setMessage: (state, message) => {
      state.message = message
    }
  },
  actions: {
    login: ({ dispatch }, user) => dispatch('updateAllState', { user }),
    logout: ({ dispatch }) => dispatch('updateAllState', {}),
    errorLogin: ({ dispatch }, message) => dispatch('updateAllState', { message }),
    updateSessionInLocalStorage: ({ state }) => updateSessionInLocalStorage(state),
    updateAllState: ({ commit }, { user = null, message = '' }) => {
      commit('setUser', user)
      commit('setMessage', message)
    },
    redirectToLogin: () => router.push({ name: 'login' })
  },
  getters: {
    user: state => state.user,
    message: state => state.message,
    isLogged: (state, { user }) => hasValue(user),
    apiKey: (state, { isLogged, user }) => isLogged ? user.api_key : null,
    shouldGoToLogin: (state, { isLogged }) => route => {
      const requiresAuth = route.matched.some(record => record.meta.requiresAuth)
      const isLogin = route.matched.some(record => record.name.login === 'login')
      return (requiresAuth && !isLogin && !isLogged)
    }
  }
}
