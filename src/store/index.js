import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import controls from './controls'
import programas from './programas'
import projects from './projects'
import loaders from './loader'
import refugios from './refugios'
import routeUtils from './route-utils'
import cota from './cota'
import issues from './issues'
import instituciones from './instituciones'
import projectsFilter from './projects-filters'
import refugiosFilter from './refugios-filters'
import institucionesFilter from './instituciones-filters'
import tab from './tab'
import searcher from './searcher'
import header from './header'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    controls,
    programas,
    projects,
    loaders,
    refugios,
    routeUtils,
    cota,
    issues,
    instituciones,
    projectsFilter,
    refugiosFilter,
    institucionesFilter,
    tab,
    searcher,
    header
  }
})

export default store
