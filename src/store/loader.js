const initialState = {
  show: false,
  config: {
    hideOverlay: false
  }
}

export default {
  state: {
    loader: {
      ...initialState
    }
  },
  mutations: {
    setLoader: (store, show) => {
      store.loader = {
        ...store.loader,
        show
      }
    },
    setLoaderConfig: (store, config) => {
      store.loader = {
        ...store.loader,
        config: {
          ...store.loader.config,
          ...config
        }
      }
    }
  },
  actions: {
    showLoader: ({ commit, state }, config) => {
      if (config) commit('setLoaderConfig', config)
      if (!state.loader.show) commit('setLoader', true)
    },
    hideLoader: ({ commit, state }, config) => {
      if (state.loader.show) commit('setLoader', false)
      if (config) commit('setLoaderConfig', config)
    }
  },
  getters: {
    httpIsProcessing: state => state.loader.show,
    loaderConfig: state => state.loader.config
  }
}
