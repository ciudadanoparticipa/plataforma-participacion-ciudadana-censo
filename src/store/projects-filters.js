import { buildActions, buildMutations } from './common-filters'
import { projectFilterObject, PROJECT_FILTER_MODULE_NAME, projectDefaultYear } from '@/constants'
import { clone } from '@/utils/object'

const moduleName = PROJECT_FILTER_MODULE_NAME

const projFilters = {
  executors: projectFilterObject.executors.map(t => t.text),
  status: projectFilterObject.status.map(t => t.value),
  types: projectFilterObject.types.map(t => t.text),
  year: projectDefaultYear
}

export default {
  state: {
    filters: {
      projects: {
        ...clone(projFilters),
        showYearLengends: true
      }
    }
  },

  mutations: {
    ...buildMutations(moduleName)
  },

  actions: {
    ...buildActions(moduleName),
    setYear: ({ dispatch }, year) => {
      dispatch('setProjectsFilter', { year, showYearLengends: true })
    },

    emptyFilterExecutors: ({ dispatch }) => dispatch('emptyProjectsFilter', { filterName: 'executors' }),
    emptyFilterStatus: ({ dispatch }) => dispatch('emptyProjectsFilter', { filterName: 'status' }),
    emptyFilterTypes: ({ dispatch }) => dispatch('emptyProjectsFilter', { filterName: 'types' }),

    reloadExecutors: ({ dispatch }) => dispatch('reloadProjectsFilter', { filterName: 'executors', fieldName: 'text' }),
    reloadStatus: ({ dispatch }) => dispatch('reloadProjectsFilter', { filterName: 'status', fieldName: 'value' }),
    reloadTypes: ({ dispatch }) => dispatch('reloadProjectsFilter', { filterName: 'types', fieldName: 'text' }),

    toggleExecutors: ({ dispatch }, executor) => dispatch('toggleProjectsFilter', { executors: executor }),
    toggleStatus: ({ dispatch }, status) => dispatch('toggleProjectsFilter', { status: status }),
    toggleTypes: ({ dispatch }, type) => dispatch('toggleProjectsFilter', { types: type }),

    hideYearLengends: ({ dispatch }) => dispatch('setProjectsFilter', { showYearLengends: false })
  },

  getters: {
    showYearLengends: state => state.showYearLengends,
    projectsFilter: state => state.filters.projects
  }
}
