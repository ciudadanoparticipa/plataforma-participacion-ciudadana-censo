import ProjectService from '@/services/projects'
import { clone } from '@/utils/object'

const projectData = {
  count: 0,
  projects: [],
  summary: {
    total: {
      cost: 0,
      families: 0,
      projects: 0
    },
    filter: {
      cost: 0,
      families: 0,
      projects: 0
    }
  }
}

export default {
  state: {
    allProjects: [],

    projects: [],

    projectInfo: null,

    data: clone(projectData)
  },

  mutations: {

    showProject: (state, project) => {
      state.projectInfo = project
    },

    setProjects: (state, projects) => {
      state.projects = projects
    },

    setData: (state, data) => {
      state.data = data
    },

    setAllProjects: (state, projects) => {
      state.allProjects = projects
    }
  },

  actions: {

    showProjectInfo: ({ commit }, project) => commit('showProject', project),

    setProjects: ({ commit, dispatch }, projects) => {
      commit('setProjects', projects)
      dispatch('filterProjects')
    },
    setAllProjects: ({ commit }, projects) => commit('setAllProjects', projects),

    loadProjectDataAndFilter: ({ dispatch }, $http) => ProjectService.getProjects($http).then(projects => {
      dispatch('setAllProjects', projects)
      dispatch('setProjects', projects)
    }),

    setProjectInfoAndUpdateRoute: ({ dispatch, getters }, { projectInfo }) => {
      dispatch('showProjectInfo', projectInfo)
      if (projectInfo) {
        if (getters.getCurrentRouteObj.query.tab) {
          dispatch('setActivateTab', getters.getCurrentRouteObj.query.tab)
        } else {
          dispatch('setActivateTab', 'descripcion')
        }
      }
    },

    filterProjects ({ commit, dispatch, getters, state }) {
      commit('setData', ProjectService.filterProjects(state.projects, getters.projectsFilter))
      if (getters.isDetailView) return
      dispatch('setProjectInfoAndUpdateRouteWhenFilterProjects')
    },

    setProjectInfoAndUpdateRouteWhenFilterProjects: ({ dispatch, getters }) => {
      if (!getters.getProjectInfoInFilterList) {
        dispatch('setProjectInfoAndUpdateRoute', { projectInfo: null })
      }
    }
  },

  getters: {

    data: state => state.data,

    dataWithShapesByProjectList: () => projects => {
      if (projects && projects.length) {
        return Promise.all(projects.map(ProjectService.addGeoData))
          .then(projects => {
            if (projects && projects.length) return projects
            return []
          })
      } else return Promise.resolve([])
    },

    dataWithShapes: (state, getters) => {
      const data = getters.data
      if (data.projects && data.projects.length) {
        return getters.dataWithShapesByProjectList(data.projects)
          .then(projects => {
            return {
              ...data,
              projects
            }
          })
      } else {
        return Promise.resolve(data)
      }
    },

    getProjectInfo: (state) => state.projectInfo,

    allProjects: state => state.allProjects,

    summary: (state, getters) => getters.data.summary,

    getAllSubprojects: state => ProjectService.getSubprojectList,

    getNewNumber: state => $http => ProjectService.getNewNumber($http),

    getProjectInfoInFilterList: (state, getters) => getters.getProjectInfo && getters.data.projects.find(project => project.code === getters.getProjectInfo.code)
  }
}
