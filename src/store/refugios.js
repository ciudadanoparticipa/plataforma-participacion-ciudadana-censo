import { hasValue, getData } from '@/utils/object.js'
import { emptyGeoJson, refugioDefaultYear } from '@/constants'
import { hasValue as hasValueInArray } from '@/utils/array'
import RefugiosService from '@/services/refugios'
import router from '@/router'

export default {
  state: {
    allRefugiosFeatures: [],
    refugios: {},
    refugiosYear: refugioDefaultYear,
    currentRefugio: null,
    firstTime: {
      refugioNro: null
    },
    graficoData: null,
    censo: {
      byYear: {},
      byYearAndRefugio: {}
    },
    evaluaciones: {},
    imagenes: {}
  },
  mutations: {
    setRefugios: (state, refugios) => { state.refugios = refugios },
    setAllRefugiosFeatures: (state, features) => { state.allRefugiosFeatures = features },
    setRefugiosYear: (state, year) => { state.refugiosYear = year },
    setCurrentRefugio: (state, refugio) => { state.currentRefugio = refugio },
    setFirstTimeRefugioNro: (state, refugioNro) => { state.firstTime.refugioNro = refugioNro },
    setGraficoData: (state, graficoData) => { state.graficoData = graficoData },
    setCensoByYear: (state, censo) => {
      state.censo = {
        ...state.censo,
        byYear: censo
      }
    },
    setCensoByYearAndNroRefugio: (state, censo) => {
      state.censo = {
        ...state.censo,
        byYearAndRefugio: censo
      }
    },
    setEvaluaciones: (state, evaluaciones) => {
      state.evaluaciones = evaluaciones
    },
    setImagenes: (state, imagenes) => {
      state.imagenes = imagenes
    }
  },
  actions: {
    setRefugios: ({ commit, dispatch }, { refugios, shouldFilter = true }) => {
      commit('setRefugios', refugios)
      if (shouldFilter) dispatch('filterRefugios')
      if (!(refugios && hasValueInArray(refugios.features))) dispatch('setCurrentRefugioAndUpdateRoute', {})
    },
    setAllRefugiosFeatures: ({ commit }, features) => commit('setAllRefugiosFeatures', features),
    setRefugiosYear: ({ commit, dispatch }, year) => {
      commit('setRefugiosYear', year)
      dispatch('loadRefugios', year)
      dispatch('loadAllCensoData')
    },
    setCurrentRefugio: ({ commit, dispatch }, refugio) => {
      if (!refugio) {
        commit('setCurrentRefugio', null)
        return
      }
      commit('setCurrentRefugio', refugio)
      dispatch('loadCensoByYearAndNroRefugio')
    },
    setCurrentRefugioAndUpdateRoute: ({ dispatch }, { refugio }) => {
      if (refugio) router.replace(`/map?ver=refugios&refugio_nro=${refugio.nro}`)
      else router.replace(`/map?ver=refugios`)
      dispatch('setCurrentRefugio', refugio)
    },
    setCurrentRefugioByNro: ({ commit, dispatch, state }, refugioNro) => {
      const refugio = RefugiosService.getRefugiosByNro(state.refugios, refugioNro)
      if (hasValue(refugio)) {
        dispatch('setCurrentRefugio', refugio)
        dispatch('loadCensoByYearAndNroRefugio')
      } else {
        commit('setFirstTimeRefugioNro', refugioNro)
      }
    },

    loadAllDataInRefugios: ({ dispatch }, year) => {
      dispatch('loadRefugios', year)
      dispatch('loadGraficoData')
      dispatch('loadAllCensoData')
      dispatch('loadEvaluaciones')
      dispatch('loadImagenes')
    },

    loadRefugios: ({ commit, dispatch, state, rootState }, year) => RefugiosService.getRefugios(year).then(refugios => {
      dispatch('setAllRefugiosFeatures', refugios.features)
      if (rootState.controls.interface.state.refugios) dispatch('setRefugios', { refugios })
      if (state.firstTime.refugioNro) {
        const refugio = RefugiosService.getRefugiosByNro(refugios, state.firstTime.refugioNro)
        if (hasValue(refugio)) {
          dispatch('setCurrentRefugio', refugio)
          commit('setFirstTimeRefugioNro', null)
        }
      }
    }),

    loadGraficoData: ({ commit }) => RefugiosService.getGraficoData().then(data => commit('setGraficoData', data)),
    loadAllCensoData: ({ dispatch }) => {
      dispatch('loadCensoByYear')
      dispatch('loadCensoByYearAndNroRefugio')
    },
    loadCensoByYear: ({ commit, state }) => RefugiosService.getCensoByYear(state.refugiosYear).then(data => commit('setCensoByYear', data)),

    loadCensoByYearAndNroRefugio: ({ commit, state }) => {
      if (state.currentRefugio) {
        RefugiosService.getCensoByYearAndNroRefugio(state.refugiosYear, state.currentRefugio.nro).then(data => commit('setCensoByYearAndNroRefugio', data))
      }
    },

    loadEvaluaciones: ({ commit }) => RefugiosService.getEvaluaciones().then(data => commit('setEvaluaciones', data)),

    loadImagenes: ({ commit }) => RefugiosService.getImagenesRefugios().then(data => commit('setImagenes', data)),

    filterRefugios: ({ commit, state, getters }) => {
      RefugiosService.filterRefugios(state.allRefugiosFeatures, getters.refugiosFilter, getters.refugiosYear).then(features => {
        commit('setRefugios', {
          ...state.refugios,
          features
        })
      })
    },

    activeOrDeactiveRefugios: ({ dispatch, getters }, controlVal) => {
      if (controlVal) {
        dispatch('loadRefugios', getters.refugiosYear)
      } else {
        dispatch('setRefugios', { refugios: emptyGeoJson, shouldFilter: false })
      }
    }
  },
  getters: {
    refugios: state => state.refugios,
    refugiosCensados: state => RefugiosService.getRefugiosCensados(state.refugios),
    refugiosByCentroMunicipalId: state => id => RefugiosService.getRefugiosByCentroMunicipalId(state.allRefugiosFeatures, id),
    refugiosYear: state => state.refugiosYear,
    currentRefugio: state => state.currentRefugio,
    getAllRefugios: state => RefugiosService.getRefugios(),
    barriosRefugiosHistoria: state => RefugiosService.getBarriosRefugiosHistoria(),
    barriosByRefugio: state => state.currentRefugio && RefugiosService.getBarriosRefugiosHistoriaByRefugioNro(state.currentRefugio.nro),
    getAreaByCurrentRefugio: state => RefugiosService.getAreaRefugiosByRefugioNro(getData(state.currentRefugio).nro),
    censo: state => state.censo.byYearAndRefugio,
    censoByCurrentYear: state => state.censo.byYear,
    thereIsCensoForRefugioNro: (state, getters) => refugioNro => !!getters.getCensoByRefugioNro(refugioNro),
    getCensoByRefugioNro: state => refugioNro => getData(state.censo.byYear)[refugioNro],
    getCantidadFamiliasInCensoByRefugioNro: (state, getters) => refugioNro => getData(getters.getCensoByRefugioNro(refugioNro)).familias || 0,
    getCantidadPersonasInCensoByRefugioNro: (state, getters) => refugioNro => getData(getData(getters.getCensoByRefugioNro(refugioNro)).personas).total || 0,
    getPromedioFechaMudanzaInCensoByRefugioNro: (state, getters) => refugioNro =>
      RefugiosService.getPromedioFechaMudanzaInDays(getData(getters.getCensoByRefugioNro(refugioNro)).promedioFechaMudanza),
    grafico: state => state.graficoData,
    getFuenteRefugioCenso: (state, getters) => RefugiosService.getFuenteRefugioCensoByYear(getters.refugiosYear),
    getFuenteRefugioCensoByYear: state => year => RefugiosService.getFuenteRefugioCensoByYear(year),
    totalBanos: state => state.evaluaciones.total_baños,
    totalDuchas: state => state.evaluaciones.total_duchas,
    evaluacionesByRefugios: state => state.evaluaciones.evaluacion || {},
    evaluacionByCurrentRefugioNro: (state, getters) => state.currentRefugio && getters.evaluacionesByRefugios[state.currentRefugio.nro],
    imagenes: state => state.imagenes,
    imagenesByCurrentRefugioNro: (state, getters) => state.currentRefugio && RefugiosService.getImagenesByRefugios(getters.imagenes, state.currentRefugio.nro)
  }
}
