import RedmineService from '@/services/redmine-gateway'

/**
    This store is used to bring trackers (used to classify issues, the options are
    shown to the user in the form) and to submit form contents.
*/
export default {
  state: {
    trackers: []
  },
  mutations: {
    setTrackers (state, arg) { state.trackers = arg }
  },
  getters: {
    getTrackers: (state) => state.trackers,
    getTrackerIdByName: (state) => (name) => {
      let id = -1
      for (var i = 0; i < state.trackers.length; i++) {
        if (state.trackers[i].name === name) {
          id = state.trackers[i].id
        }
      }
      return id
    }
  },
  actions: {

    loadTrackers: ({ commit }, $http) => RedmineService.getTrackers($http).then(trackers => commit('setTrackers', trackers)),

    createIssue (state, { http, data }) {
      let newIssue = {
        'project_id': data.project_id,
        'subject': `Mensaje de ${data.name} - ${data.email}`,
        'tipoParticipacion': data.messageType,
        'name': data.name,
        'email': data.email,
        'latitude': data.latitude,
        'longitude': data.longitude
      }

      // add tipo de reclamo, if field is not empty
      if (data.tipoReclamo) {
        if (data.tipoReclamo === 'Otro') { newIssue.tipoReclamo = data.tipoReclamoOtro } else { newIssue.tipoReclamo = data.tipoReclamo }
      }
      if (data.otroTitulo) {
        // if tipoParticipacion='otro', save the title before the message
        newIssue.message = `${data.otroTitulo}\n\n${data.message}`
      } else {
        newIssue.message = data.message
      }

      return RedmineService.createIssue(http, newIssue, data.archivos)
    }
  }
}
