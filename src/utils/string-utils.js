const accentMap = {
  'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'ñ': 'n'
}

export const accentFold = str => {
  if (!str) return ''
  let ret = ''
  for (let i = 0, len = str.length; i < len; i++) {
    ret += accentMap[str.charAt(i)] || str.charAt(i)
  }
  return ret
}

export const lower = str => str ? str.toLowerCase() : ''

export const upper = str => str ? str.toUpperCase() : ''

export const compareIgnoringAccent = (str1, str2) => accentFold(lower(str1)) === accentFold(lower(str2))

export const capitalize = str => str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1))

export const spaceToUnderscore = str => str.replace(/ /g, '_')

export const camelToUnderscoreCase = str => str.replace(/\.?([A-Z])/g, (x, y) => '_' + y.toLowerCase()).replace(/^_/, '')

export const stringify = obj => obj ? JSON.stringify(obj, null, 2) : ''
