import L from 'leaflet'

export const buildDivIcon = ({ className, html, iconSize }) => L.divIcon({ className, html, iconSize })

export const buildSmallIcon = ({ color, number, extraSlot = '' }) => {
  return buildDivIcon({
    className: `makerIcon`,
    html: `
      <div class="icon-classify project-${number} ap-pin ${number > 9 ? 'gt9' : ''}" style="color: ${color}; font-size: 50px;">
        <span class="project-status project-number">${number}</span>
        ${extraSlot}
      </div>`,
    iconSize: [50, 50]
  })
}

export const buildSelectedIcon = ({ color, number }) => {
  return buildDivIcon({
    className: 'makerIcon makerIcon-selected',
    /* Clase para clasificar cada marker: ${this.projectInfo.statusByYear ? 'ap-' + this.projectInfo.statusByYear[this.selectedYear] + '-phase': ''} */
    html: `
      <div class="icon-classify ap-pin-double ${number > 9 ? 'gt9' : ''}" style="color: ${color}; font-size: 120px;">
        <span class="project-status project-number">${number}</span>
      </div>`,
    iconSize: [120, 120]
  })
}

export const getDefaultLayerStyle = feature => {
  return {
    color: feature.properties.color,
    dashArray: feature.properties.lineType,
    fillOpacity: feature.properties.opacity || 0.5,
    weight: 2
  }
}
