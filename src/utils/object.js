export const hasValue = obj => !!obj && !!Object.values(obj).length
export const clone = obj => obj ? JSON.parse(JSON.stringify(obj)) : {}
export const hasData = obj => hasValue(obj) && Object.values(obj).reduce((p, c) => p || !!c, false)
export const getData = obj => hasValue(obj) ? obj : {}
