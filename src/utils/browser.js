const objJSONForIE11 = (url) => {
  return new Promise((resolve, reject) => {
    let obj = new XMLHttpRequest()
    if (window.ActiveXObject) obj = new window.ActiveXObject('Microsoft.XMLHTTP')
    obj.open('get', url, true)
    obj.responseType = 'json'
    obj.onload = () => {
      if (obj.status === 200) resolve(obj)
      else reject(status)
    }
    obj.send()
  })
}

const isEdgeBrowser = () => {
  // in IE 11 the navigator.appVersion says 'trident'
  // in Edge the navigator.appVersion does not say trident
  if (navigator.appName !== 'Microsoft Internet Explorer' && navigator.appName === 'Netscape' && navigator.appVersion.indexOf('Edge') !== -1) return true
  return false
}

export const isIE11 = !!window.MSInputMethodContext && !!document.documentMode
export const isEdge = isEdgeBrowser()

export const fetch = (isIE11 || isEdge) ? objJSONForIE11 : window.fetch
