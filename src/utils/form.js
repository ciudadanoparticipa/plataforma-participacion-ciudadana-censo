export const clone = obj => obj ? JSON.parse(JSON.stringify(obj, null, 2)) : {}
export const changeValueBy = (obj, oldVal, curVal) => {
  if (!obj) return {}
  let objClone = clone(obj)
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'object') {
      objClone = {
        ...objClone,
        [key]: changeValueBy(obj[key], oldVal, curVal)
      }
    } else {
      let comparateValue = (typeof obj[key] !== 'number') ? parseFloat(obj[key]) : obj[key]
      objClone = {
        ...objClone,
        [key]: comparateValue === oldVal ? curVal : obj[key]
      }
    }
  })
  return objClone
}

export const whenValueIsMinusOneChangeByCero = obj => {
  return changeValueBy(obj, -1, 0)
}

export const whenValueIsCeroChangeByMinusOne = obj => {
  return changeValueBy(obj, 0, -1)
}

export const parseFloatDeep = (obj) => {
  let objClone = clone(obj)
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'object') {
      objClone = {
        ...objClone,
        [key]: parseFloatDeep(obj[key])
      }
    } else {
      objClone = {
        ...objClone,
        [key]: (typeof obj[key] !== 'number') ? parseFloat(obj[key]) : obj[key]
      }
    }
  })
  return objClone
}

export const formatDate = (date = new Date()) => `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDay()} a las ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} horas - time: ${date.getTime()}`
