export const parseNumber = num => num ? typeof num === 'string' ? (parseFloat(num.replace(/[\D\s._-]+/g, '').replace(/,/g, '.')) || 0) : num : 0
export const numToLocateEs = num => Intl.NumberFormat('es').format(num)

export const round = (numero, decimales = 2, usarComa = true) => {
  if (isNaN(numero)) return 0
  const opciones = {
    maximumFractionDigits: decimales,
    useGrouping: false
  }
  usarComa = usarComa ? 'es' : 'en'
  return new Intl.NumberFormat(usarComa, opciones).format(numero)
}

export const thousandSeparatorFormat = value => {
  if (!value) return
  // clean number of strange characters
  let number = parseNumber(value)
  return numToLocateEs(number)
}

export const inputValueThousandSeparatorFormat = event => {
  // When user select text in the document, also abort.
  const selection = window.getSelection().toString()
  if (selection !== '') return
  // When the arrow keys are pressed, abort.
  if ([38, 40, 37, 39].find(num => event.keyCode === num)) return
  const $this = event.target || event.srcElement
  // Get the value.
  $this.value = $this.value ? thousandSeparatorFormat($this.value) : ''
  return $this.value
}
