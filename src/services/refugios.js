import { clone, getData as getObj } from '@/utils/object'
import { accentFold, lower } from '@/utils/string-utils'
import { getData } from '@/services/commons'
import { estadoRefugios, zonaRefugios, barriosRefugios, mapBetweenComposicionFamiliarAndCensoKeys, fuenteRefugioCensoByYear } from '@/constants'

let censoCachedData = null
let barriosRefugiosCachedData = null
let imagenesRefugiosCachedData = null
let evaluacionRefugiosCachedData = null

const getFiltersForCompare = filter => ({
  ...filter,
  barrios: filter.barrios.map(b => accentFold(lower(b))).map(b => b === lower(barriosRefugios.noDefinido) ? '' : b),
  zonas: filter.zonas.map(lower).map(z => z === lower(zonaRefugios.noDefinida) ? '' : z)
})

const filterRefugioByStatus = (filterStatus, refugioStatus) => filterStatus.indexOf(refugioStatus) >= 0
const filterRefugioByZonas = (filterZonas, refugioZona) => filterZonas.indexOf(lower(refugioZona).trim()) >= 0
const filterRefugioByBarrios = (filterBarrios, refugioBarrio) => filterBarrios.indexOf(accentFold(lower(refugioBarrio))) >= 0
const filterRefugioByCensoStatus = refugio => filterRefugioByStatus([estadoRefugios.censado], refugio.estadoCenso)

const filterRefugioByAllData = (refugio, filter) => {
  return filterRefugioByStatus(filter.refugiosStatus, refugio.estadoCenso) &&
    filterRefugioByZonas(filter.zonas, refugio.zona_banhado) &&
    filterRefugioByBarrios(filter.barrios, refugio.barrio)
}

const filterRefugioByComposicionFamiliar = (filterComposicionFamiliar, censo) => filterComposicionFamiliar.map(key => {
  if (key === 'Adolecentes') return true
  return censo[mapBetweenComposicionFamiliarAndCensoKeys[key]]
}).reduce((prev, curr) => prev || !!curr, false)

const fuenteRefugioCensoByYearFunction = (year, updatedAt) => {
  if (year < 2018) return fuenteRefugioCensoByYear[year]
  return `${fuenteRefugioCensoByYear['updatedAt']} ${updatedAt}`
}

export default class RefugiosService {
  static getRefugios (year) {
    return Promise.all(
      [
        getData(`/data/refugios/refugios.json`),
        RefugiosService.getCensoByYear(year)
      ]
    ).then(([ ref, censo ]) => {
      const features = ref && ref.features ? ref.features : []
      return {
        ...ref,
        features: features
          .filter(f => f.geometry)
          .map(f => {
            const nro = f.properties.nro
            if (getObj(censo)[nro]) f.properties.estadoCenso = estadoRefugios.censado
            else f.properties.estadoCenso = estadoRefugios.noCensado
            return f
          })
          .sort((r1, r2) => {
            const first = r1.properties.nombre.trim()
            const second = r2.properties.nombre.trim()
            if (first < second) { return -1 }
            if (first > second) { return 1 }
            return 0
          })
      }
    })
  }

  static getRefugiosByNro (refugios, nro) {
    if (refugios && refugios.features && refugios.features.length) {
      const { properties } = refugios.features.find(r => r.properties.nro === nro)
      return properties
    }
    return null
  }

  static getRefugiosCensados (refugiosGeoJson) {
    const ref = clone(refugiosGeoJson)
    if (!ref.features) return { ...ref, features: [] }
    return {
      ...ref,
      features: ref.features.filter(f => filterRefugioByCensoStatus(f.properties))
    }
  }

  static searchRefugios (refugiosGeoJson, search) {
    const filter = search.toUpperCase()
    const ref = clone(refugiosGeoJson)
    return {
      ...ref,
      features: ref.features.filter(f => f.properties.nombre.toUpperCase().indexOf(filter) > -1)
    }
  }

  static getRefugiosByCentroMunicipalId (refugios, centroMunicipalId) {
    return refugios.map(r => r.properties).filter(r => parseInt(r.centro_municipal) === centroMunicipalId)
  }

  static getBarriosRefugiosHistoria () {
    if (barriosRefugiosCachedData) return Promise.resolve(barriosRefugiosCachedData)
    return getData(`/data/refugios/barrios_refugios.json`).then(data => {
      barriosRefugiosCachedData = data
      return data
    })
  }

  static getBarriosRefugiosHistoriaByRefugioNro (nro) {
    return RefugiosService.getBarriosRefugiosHistoria().then(barrios => ({
      ...barrios,
      features: barrios.features.filter(f => f.properties.nro === nro)
    }))
  }

  static getAreaRefugios () {
    return getData(`/data/refugios/refugios_areas.json`)
  }

  static getAreaRefugiosByRefugioNro (nro) {
    return RefugiosService.getAreaRefugios().then(areas => {
      const features = areas.features ? areas.features.filter(f => f.properties.nro === nro) : []
      return { ...areas, features }
    })
  }

  static getAllCenso () {
    if (censoCachedData) return Promise.resolve(censoCachedData)
    return getData(`/data/refugios/censo.json`).then(censo => {
      censoCachedData = censo
      return censo
    })
  }

  static getCensoByYear (year) {
    return RefugiosService.getAllCenso().then(censo => censo[year])
  }

  static getCensoByYearAndNroRefugio (year, nro) {
    return RefugiosService.getCensoByYear(year).then(censo => censo[nro])
  }

  static getGraficoData () {
    return getData(`/data/refugios/grafico.json`)
  }

  static filterRefugios (refugioFeatures, filter, year) {
    return new Promise(resolve => {
      const filters = getFiltersForCompare(filter)
      const arr = refugioFeatures.filter(r => filterRefugioByAllData(r.properties, filters))
      if (filters.refugiosStatus.indexOf(estadoRefugios.censado) >= 0) {
        RefugiosService.getCensoByYear(year).then(allCensos => {
          let refugiosFilteredArr = []
          arr.forEach(refugio => {
            const censo = allCensos[refugio.properties.nro]
            if (censo) {
              if (filterRefugioByComposicionFamiliar(filters.composicionFamiliar, censo)) {
                refugiosFilteredArr.push(refugio)
              }
            } else if (filters.refugiosStatus.indexOf(estadoRefugios.noCensado) >= 0) {
              refugiosFilteredArr.push(refugio)
            }
          })
          return refugiosFilteredArr
        }).then(allRefFiltered => {
          resolve(allRefFiltered)
        })
      } else {
        resolve(arr)
      }
    })
  }

  static getPromedioFechaMudanzaInDays (fechaStr) {
    if (!fechaStr) return '-'
    const oneDay = 24 * 60 * 60 * 1000 // hours  *minutes * seconds * milliseconds
    const firstDate = new Date()
    const secondDate = new Date(`${fechaStr}T00:00:00-04:00`)
    return `${Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)))} días`
  }

  static getFuenteRefugioCensoByYear (year) {
    return RefugiosService.getAllCenso().then(censo => fuenteRefugioCensoByYearFunction(year, censo.actualizacion))
  }

  static getEvaluaciones () {
    if (evaluacionRefugiosCachedData) return Promise.resolve(evaluacionRefugiosCachedData)
    return getData(`/data/refugios/evaluacion_albergues.json`).then(data => {
      evaluacionRefugiosCachedData = data
      return evaluacionRefugiosCachedData
    })
  }

  static getImagenesRefugios () {
    if (imagenesRefugiosCachedData) return Promise.resolve(imagenesRefugiosCachedData)
    return getData(`/data/refugios/albergues_imagenes.json`).then(data => {
      imagenesRefugiosCachedData = data
      return data
    })
  }

  static getImagenesByRefugios (imagenes, nro) {
    return imagenes && Array.isArray(imagenes) && imagenes.find(r => r.nro === nro)
  }
}
