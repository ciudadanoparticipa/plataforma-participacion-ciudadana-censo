const institucionesAfectadas = require(`@/assets/json/cotas/instituciones_afectadas.json`)

const getFileForCota = cota => {
  try {
    return require(`@/assets/json/cotas/cota_${cota}.json`)
  } catch (e) {
    console.warn(`El path "@/assets/json/cotas/cota_${cota}.json" es incorrecto o no existe el archivo`, e)
    return null
  }
}

export default class CotaService {
  static getZonasInundables (cota) {
    if (!cota) return null
    return {
      sur: getFileForCota(`${cota}_sur`),
      norte: getFileForCota(`${cota}_norte`)
    }
  }

  static getInstitucionesAfectadas (cota) {
    if (!cota) return null
    return institucionesAfectadas[`cota${cota}`]
  }
}
