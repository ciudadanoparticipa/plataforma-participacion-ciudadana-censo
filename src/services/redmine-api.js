import { getResponse } from '@/services/commons'

let url = `${process.env.VUE_APP_BASE_URL}redmineapi/`

const options = {
  headers: {
    'Content-Type': 'application/json'
  }
}

export default class RedmineService {
  static getIssues (http, token, queryString = '') {
    return getResponse(http.get(`${url}issues.json?key=${token}&limit=100${queryString}`, options))
      .then(data => data.issues.map(issue => {
        const latitude = issue.custom_fields.find(c => c.name === 'Latitud')
        const longitude = issue.custom_fields.find(c => c.name === 'Longitud')
        issue.latitude = latitude ? latitude.value : ''
        issue.longitude = longitude ? longitude.value : ''
        return issue
      })
        .map(issue => {
          if (typeof issue.latitude === 'string') {
            issue.latitude = parseFloat(issue.latitude)
          }
          if (typeof issue.longitude === 'string') {
            issue.longitude = parseFloat(issue.longitude)
          }
          return issue
        })
        .filter(issue => issue.latitude && issue.longitude)
      )
  }

  static getProjects (http, token) {
    return getResponse(http.get(`${url}projects.json?key=${token}`))
      .then(data => data.projects)
      .then(projects => projects
        .filter(project => project.status !== 5)
        .map(project => {
          const projectInfo = project.custom_fields.find(cf => cf.name === 'projectInfo')
          project.projectInfo = projectInfo && projectInfo.value ? JSON.parse(projectInfo.value) : ''
          project.projectInfo.id = project.id
          project.projectInfo.code = project.identifier
          project.projectInfo.identifier = project.identifier
          project.projectInfo.assigned_to_id = project.assigned_to_id
          project.latitude = project.projectInfo.mapProperties.mapIconPosition[1]
          project.longitude = project.projectInfo.mapProperties.mapIconPosition[0]
          return project
        })
      )
  }

  static getProjectsWithIssues (http, token) {
    return RedmineService.getProjects(http, token)
      .then(projects => projects
        .map(project => RedmineService.getIssues(http, token, `&project_id=${project.id}`).then(issues => {
          project.issues = issues
          return project
        }))
      )
      .then(allPromises => Promise.all(allPromises))
  }
}
