import { institucionesTypes } from '@/constants'
import { getData } from '@/services/commons'
import { getArray } from '@/utils/array'
import { getData as getObject } from '@/utils/object'

const filterRefugioByInstitucionesTypes = (filterInstitucionesTypes, institucionesTypes) => filterInstitucionesTypes.indexOf(institucionesTypes) >= 0

const filterInstitucionByAllData = (institucion, filter) => {
  return filterRefugioByInstitucionesTypes(filter.institucionesTypes, institucion.tipo_institucion)
}

export default class InstitucionesService {
  static getCentrosMunicipales () {
    return getData(`/data/instituciones/centros_municipales.json`)
  }

  static getCentroMunicipalById (list, id) {
    return getObject(getArray(list).find(c => getObject(c.properties).id === parseInt(id))).properties
  }

  static getAllInstituciones () {
    return getData(`/data/instituciones/instituciones.json`)
  }

  static getInstituciones () {
    return InstitucionesService.getAllInstituciones().then(instituciones => {
      const features =
        instituciones.features
          ? instituciones.features.filter(i => i.properties.tipo_institucion && i.properties.tipo_institucion !== 'Centros Municipales')
          : []
      return {
        ...instituciones,
        features
      }
    })
  }

  static getInstitucionById (list, institucionId) {
    const id = typeof institucionId === 'string' ? parseInt(institucionId) : institucionId
    return getObject(getArray(list).find(c => getObject(c.properties).id === id)).properties
  }

  static getCoberturasDeCentrosMunicipales () {
    return Promise.all([
      getData(`/data/instituciones/coberturas/cobertura_cm1.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm2.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm3.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm4.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm5.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm6.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm7.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm8.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm9.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm10.json`),
      getData(`/data/instituciones/coberturas/cobertura_cm11.json`)
    ]).then(([cm1, cm2, cm3, cm4, cm5, cm6, cm7, cm8, cm9, cm10, cm11]) => ({
      cm1, cm2, cm3, cm4, cm5, cm6, cm7, cm8, cm9, cm10, cm11
    }))
  }

  static getCoberturaByCentroMunicipal (coberturas, cmNro) {
    return getObject(coberturas)[`cm${cmNro}`]
  }

  static filterCentrosMunicipales (features, filters) {
    return new Promise(resolve => resolve(features.filter(() => filterRefugioByInstitucionesTypes(filters.institucionesTypes, institucionesTypes.centrosMunicipales))))
  }

  static filterInstituciones (features, filters) {
    return new Promise(resolve => resolve(features.filter(r => filterInstitucionByAllData(r.properties, filters))))
  }
}
