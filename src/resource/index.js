import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.http.interceptors.push(function (request, next) {
  const store = this.$store || { dispatch: () => null }
  request.headers.set('Content-Type', 'application/json')
  store.dispatch('showLoader')
  next(res => {
    setTimeout(() => store.dispatch('hideLoader'), 1000)
    return res
  })
})

// Vue.http.interceptor.before = ((request, next) => {
//     console.log("Vue.http.interceptor.before", request, this)
//     next(res => {
//         console.log("RESP 2", res)
//         // res.data = { 'hola': 'quetal'}
//         return res
//     })
// })
