# Plataforma de Participación Ciudadana

## Instalar dependencias
```
npm install
```

### Compilar para desarrollo
```
npm run serve
```

### Compilar y minificar para producción
```
npm run build
```

### Ejecutar test
```
npm run test
```

### Verificar y corregir código fuente
```
npm run lint
```

### Ejecutar test de integración
```
npm run test:e2e
```

### Ejecutar test unitarios
```
npm run test:unit
```

### Personalizar configuración
See [Configuration Reference](https://cli.vuejs.org/config/).

---
## Servicios externos

### Envio de mail

* Post https://getsimpleform.com/messages?api_token=64985e2fdaef1d34a62f4fe3da5500bc
* Email: participacion-ciudadana-no-reply@gmail.com
---
# Configuración para producción

## 1. Clonar el proyecto en la máquina host de producción

**Obs**: A partir de ahora cuando nos referimos a `$HOME_PROYECTO` sería el directorio donde hicimos el `git clone` y el nombre del proyecto.

Ejemplo:
```
HOME_PROYECTO=/home/user/plataforma-participacion-ciudadana
```
Siendo:
* El directorio donde hicimos el clone: `/home/user`
* El nombre del proyecto `plataforma-participacion-ciudadana`

## 2. Compilar proyecto

Para compilar el proyecto debemos ejecutar los siguientes comandos en la terminal

```
cd plataforma-participacion-ciudadana \
&& npm install \
&& npm run build
```

## 3. Crear las imágenes docker

Se debe crear las imágenes de docker para cada uno de los servicios (Plataforma web y Redmine) utilizando el registry de Gitlab. A continuación los pasos para la creación de imágenes:

3.1. Ingresar a la opción `Packages`

  ![imagen](/README/img1.png)
  ![imagen](/README/img2.png)

3.2. Luego click en la opción `Container Registry`

  ![imagen](/README/img3.png)

3.3. Login al registry del gitlab

Ingresar a la terminal y ejecutar el siguiente comando:
```
docker login registry.gitlab.com
```
Nos va pedir ingresar las credenciales de nuestro usuario del gitlab, completamos y nos muestra el mensaje de `Login Success`

3.3. Copiar los 2 comandos mostrados y ejecutarlos en la consola, debe hacer esto para crear las imágenes de los 4 container del `docker-compose.yml`

**Obs:** Los nombres de las imágenes creadas tienen las siguientes reglas:

* Nomenclatura: `${URL_del_repositorio_de_imagenes}/${nombre_del_grupo}/${nombre_del_proyecto}:${tag_o_version}`.

  Ejemplo: registry.example.com/grupo/proyecto:version
* Si se tiene varios proyectos en un solo repositorio se puede crear imagenes como si fueran subproyectos con la siguiente nomenclatura:
`${URL_del_repositorio_de_imagenes}/${nombre_del_grupo}/${nombre_del_proyecto}/${nombre_del_subproyecto}:${tag_o_version}`.

  Ejemplo: registry.example.com/grupo/proyecto/subproyecto:version

Para más información ingrese [aquí](https://gitlab.com/help/user/project/container_registry)

3.4. En este repositorio existen 3 fuentes de las 4 imagenes que se necesita (web, api y backups_server), faltaría crear la imagen del redmine, para ello se debe hacer todos los pasos mencionados desde el 3.1. en el repostorio del redmine.

3.5. Una vez creada las 4 imagenes, copiar las URLs de las mismas y reemplazarlos en los siguientes archivos `docker-compose.yml` para sus respectivos servicios:

* [`Este repositorio`]/docker-compose.yml
* [`Este repositorio`]/production/docker-compose.yml
* [`Repositorio del Redmine`]/docker-compose.yml

## 4. Instalar los ejecutables a utilizarse (En servidores de producción y desarrollo)

Instalar los ejecutables de la carpeta `installer` de `$HOME_PROYECTO` en el orden en el que se enumeran

**Obs:** Si `5.webhook.sh` falla al ejecutarse probar con el script `5.go-and-webhook.sh`. Una vez terminada las instalaciones, cerrar la terminal y volverla a abrir

## 5. Configurar el Jenkins (En servidor de producción)

Para configurarlo seguir los pasos de esta [documentación](https://docs.google.com/document/d/1mZMwg91ziTOKgU3xtFR4KhxfLyg3k4cIQX37x6N2-yE/edit?usp=sharing)

## 6. Configuración de webhooks (En servidores de producción y desarrollo)

**Obs:** Webhooks nos ayudará para hacer deploys periódicos

6.1. En el archivo `hooks.json` de la carpeta `./production/webhooks` reemplazar la variable `$HOME_PROYECTO` por el directorio del proyecto

6.2. Hacer el mismo reemplazo de la variable `$HOME_PROYECTO` el archivo `run.sh` de la misma carpeta

6.3. Dar permisos de ejecucion `chmod +x` a los archivos `run.sh` y `script.sh`

6.4. Ejecutar el webhooks con el siguiente comando en la consola `./production/webhooks/run.sh`

## 7. Cambiar variables de entorno en el archivo Jenkinsfile

El el archivo `Jenkinsfile` de este proyecto (y el del Redmine) cambiar las variables `REGISTRY_IMAGE_URL` y `WEBHOOKS_URL`

**Obs:**

* La variable `REGISTRY_IMAGE_URL` debe poseer la url del registry. Para más información ingrese a [Gitlab Registry](https://gitlab.com/help/user/project/container_registry)
* La variable `WEBHOOKS_URL` debe poseer la url o ip pública el servidor, tal y como se configuró en el paso anterior. Ejemplo: `WEBHOOKS_URL`="http://172.168.15.75:9999"

## 8. Configuración de backups (En servidores de producción y desarrollo)

Para realizar backups diarios de la base de datos se debe:
1. Abrir un editor de texto
2. Copiar el siguiente fragmento de código en el editor
    ```
    0 2 * * * sh /$HOME_PROYECTO/production/backup-script-db.sh
    0 2 * * * sh /$HOME_PROYECTO/production/backup-script-api.sh
    ```
3. Reemplazar `$HOME_PROYECTO` por el directorio del proyecto
4. Ejecutar `cronjob -e`, y pegar en la última línea el código editado

**Obs:** Dentro de ambos archivos `backup-script-db.sh` y `backup-script-api.sh` hay variables de entorno a configurar en caso de que se desee personalizar o cambiar el funcionamiento explicado en este documento, por ejemplo cambiar el nombre de la carpeta `production` hará que el nombre de los contenedores cambien y por ende se necesitará cambiar estas variables de entorno `DOCKER_DB_CONTAINER_NAME`, `DOCKER_API_CONTAINER_NAME`.

## 9. Cambiar variables del script que restaura la base de datos (En servidores de producción y desarrollo)

En el archivo `./backups_server/restore.sh` modificar las siguientes variables:
* **BACKUP_SERVER_IP:** Agregar la IP de la servidor donde se despliegan los servicios
* **DOCKER_DB_CONTAINER_NAME:** Nombre del container de la base de datos. Ejemplo: production_db_1
* **DOCKER_API_CONTAINER_NAME:** Nombre del container del api nodejs-express. Ejemplo: production_api_1

## 10. Ejecutar el script de despliegue a Producción (En servidores de producción y desarrollo)

Una vez realizado los pasos anteriores, ejecutar el script `deploy.sh` que se encuentra en la carpeta `production` de este repositorio.

```
./production/deploy.sh
```

**Obs:** (Opcional) En el archivo `deploy.sh` se podría cambiar el valor de la variable `BACKUP_SERVER_IP` en caso de necesitar cambiar el backup inicial. Apuntar a la URL del servicio `backups_server` levantado en el servidor de producción, **es importante que en la carpeta `db` del servicio `backups_server` exista un archivo llamado initial.zip**
