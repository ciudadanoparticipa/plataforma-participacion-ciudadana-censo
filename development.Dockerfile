FROM node:10

LABEL maintainer Edgar Valdez

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install && npm audit fix && npm rebuild node-sass --force

COPY . .

EXPOSE 8080

ENTRYPOINT [ "npm" ]

CMD [ "run", "build" ]
