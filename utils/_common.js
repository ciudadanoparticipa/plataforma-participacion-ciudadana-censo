const fs = require('fs')
const chalk = require('chalk')
const log = console.log
const error = console.error

/* INICIO - FUNCIONALIDADES GENERALES */

const _clone = obj => {
  try {
    return JSON.parse(JSON.stringify(obj))
  } catch (e) {
    return undefined
  }
}

const _getFile = (readerFunction, path) => {
  try {
    return readerFunction(path, 'utf8')
  } catch (e) {
    error(chalk.red(`El path ${path} es incorrecto o no existe el archivo`, e))
    process.exit(1)
  }
}

const _saveFile = (data, path) => {
  fs.writeFile(path, JSON.stringify(data, null, 2), function (err) {
    if (err) {
      return error(chalk.red(err))
    }
    log(chalk.green('The file was saved!'))
  })
}

exports.saveFile = _saveFile

exports.getFileToJSON = path => JSON.parse(_getFile(fs.readFileSync, path))

exports.getCSVFile = path => _getFile(fs.createReadStream, path)

/* FIN - FUNCIONALIDADES GENERALES */

/* INICIO - FUNCIONALIDADES PARA OBTENER DATOS DE ESTADÍSTICAS */

const getUsefulData = projectStatisticData => {
  if (projectStatisticData && projectStatisticData['DATOS DE POBLACIÓN']) {
    return {
      'projectNumber': projectStatisticData['projectNumber'],
      'poblacion': {
        'total': projectStatisticData['DATOS DE POBLACIÓN']['Población Total(*)'],
        'hombres': projectStatisticData['DATOS DE POBLACIÓN']['% Hombres'],
        'mujeres': projectStatisticData['DATOS DE POBLACIÓN']['% Mujeres'],
        '0-14': projectStatisticData['Población por grupos de edad']['% 0 a 14 años'],
        '15-64': projectStatisticData['Población por grupos de edad']['% 15 a 64 años'],
        '65': projectStatisticData['Población por grupos de edad']['% 65 años y más']
      },
      'ocupacion': {
        'dependiente': projectStatisticData['Categoría de ocupación de la población ocupada(d)']['% Dependiente'],
        'independiente': projectStatisticData['Categoría de ocupación de la población ocupada(d)']['% Independiente'],
        'noInformado': projectStatisticData['Categoría de ocupación de la población ocupada(d)']['% No informado']
      },
      'viviendas': {
        'total': projectStatisticData['DATOS DE VIVIENDAS PARTICULARES']['Viviendas particulares ocupadas con personas presentes'],
        'conAccesoA': {
          'telefono': projectStatisticData['Acceso a TIC (Tecnología de la Información y Comunicación)']['% Viviendas con teléfono fijo'],
          'celular': projectStatisticData['Acceso a TIC (Tecnología de la Información y Comunicación)']['% Viviendas con teléfono celular'],
          'computadora': projectStatisticData['Acceso a TIC (Tecnología de la Información y Comunicación)']['% Viviendas con computadora'],
          'internet': projectStatisticData['Acceso a TIC (Tecnología de la Información y Comunicación)']['% Viviendas con computadora conectada a internet']
        },
        'condicion': {
          'propia': projectStatisticData['Condición de propiedad de la vivienda']['% Es propia'],
          'alquilada': projectStatisticData['Condición de propiedad de la vivienda']['% Es alquilada'],
          'prestada': projectStatisticData['Condición de propiedad de la vivienda']['% Es prestada, la cuidan'],
          'noInformado': projectStatisticData['Condición de propiedad de la vivienda']['% No informado']
        }
      }
    }
  } else return { 'projectNumber': projectStatisticData['projectNumber'] }
}

exports.clone = obj => obj ? _clone(obj) : {}

exports.formatNumber = str => (str.trim() === '-') ? -1 : parseFloat(str.replace(/\./g, '').replace(',', '.'))

exports.adaptDataToBaseProjects = statistics => {
  Object.keys(statistics).forEach(projName => {
    if (statistics[projName]['subProjects']['General']) {
      statistics[projName] = Object.assign({}, statistics[projName], statistics[projName]['subProjects']['General'])
      delete statistics[projName]['subProjects']['General']
    }
  })
  return statistics
}

exports.getUsefulDataWithSubProject = projectStatisticData => {
  const value = getUsefulData(projectStatisticData)
  const subProjectsKeys = Object.keys(projectStatisticData['subProjects'])
  if (subProjectsKeys.length) {
    value['subProjects'] = {}
    subProjectsKeys.forEach(subprojName => {
      value['subProjects'][subprojName] = getUsefulData(projectStatisticData['subProjects'][subprojName])
    })
  }
  return value
}

/* FIN - FUNCIONALIDADES PARA OBTENER DATOS DE ESTADÍSTICAS */

/* INICIO - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES GENERAL */

const centroMunicipalesData = {
  '1': {
    numero: '1',
    nombre: 'Ita Yvate',
    direccion: 'Atenas(9na.proyectada) e/ Montevideo y Colón',
    telefono: '(021) 423–108'
  },
  '2': {
    numero: '2',
    nombre: 'Oñondivepa',
    direccion: 'Artigas y Cañadón Chaqueño',
    telefono: '(021) 297–523/4'
  },
  '3': {
    numero: '3',
    nombre: 'Jopoi',
    direccion: 'Mencia de Sanabria 271 esq.Yataity Cora',
    telefono: '(021) 310–429'
  },
  '4': {
    numero: '4',
    nombre: 'Mburucuja',
    direccion: 'Terminal de Ómnibus de Asunción',
    telefono: '(021) 551–762'
  },
  '5': {
    numero: '5',
    nombre: 'Koeti',
    direccion: 'Oliva esq.Independencia Nacional',
    telefono: '(021) 493–702'
  },
  '6': {
    numero: '6',
    nombre: 'Jeruti',
    direccion: 'Av.Carlos A.López 1527 esq.Ruíz de Arellano',
    telefono: '(021) 425–365'
  },
  '7': {
    numero: '7',
    nombre: 'Marangatu',
    direccion: 'General Santos y Concordia',
    telefono: '(021) 233–970'
  },
  '8': {
    numero: '8',
    nombre: 'Tenondete',
    direccion: 'Sargento Tomás Lombardo c / Pascual Casco',
    telefono: '(021) 275–502'
  },
  '9': {
    numero: '9',
    nombre: 'Ñasaindy',
    direccion: 'Tte.Claudio Acosta 7056 y Calle 14 Zeballos Cué',
    telefono: '(021) 276–155'
  },
  '10': {
    numero: '10',
    nombre: ' Ñepytyvo',
    direccion: 'Tomás Romero Pereira esq.Yvapuru',
    telefono: '(021) 307–826'
  }
}

const titlesMapper = {
  'Nº': 'numero',
  'Zona / Bañado': 'zona',
  'CENTRO MUNICIPAL': 'centroMunicipal',
  'Barrio o Comunidad de Origen': 'barrio',
  'Nombre del Centro Habitacional Transitorio (Refugio)': 'nombre',
  'Calle y/o Referencia': 'direccion',
  'Georeferencia': 'georeferencia',
  'Trabajo Coordinado con otras Instituciones': 'trabajoCoordinado',
  'Cantidad de familias': 'cantidadFamilias',
  'Familias Retornadas': 'familiasRetornadas',
  'Familia a Retornar': 'familiasARetornar',
  'coordinador DGRRD': 'coordinador',
  'SUB- COORDINADORES': 'subCoordinadores',
  'Obesrvación': 'obesrvacion'
}

const getCentroMunicipalObj = num => num ? centroMunicipalesData[num] ? centroMunicipalesData[num] : {} : {}

exports.refugioInitialObj = {
  numero: 0,
  zona: '',
  centroMunicipal: {
    numero: '',
    nombre: '',
    direccion: '',
    telefono: ''
  },
  barrio: '',
  nombre: '',
  estado: '',
  direccion: '',
  georeferencia: {
    Decimales: [
      0,
      0
    ],
    latitud: 0,
    longitud: 0,
    Sexagesimales: [],
    UTM: '',
    Web: ''
  },
  trabajoCoordinado: '',
  cantidadFamilias: 0,
  familiasRetornadas: '',
  familiasARetornar: '',
  coordinadores: [{ 'nombre': '', 'telefono': '' }],
  subCoordinadores: [],
  observacion: ''
}

exports.getRefugios2017Title = str => str && str.length ? titlesMapper[str] ? titlesMapper[str] : str : ''

exports.saveRefugios2017File = (refugios, path) => _saveFile(refugios, path)

exports.getGeoreferencia = geoString => {
  return geoString.split(/\n/g).map(el => el.split(':')).filter(el => !!el[0]).reduce((previous, current) => {
    let value = current[1].trim()
    if (/https/.test(current[0])) {
      previous['Web'] = `${current[0]}:${value}`
    } else {
      previous[current[0]] = /,/.test(value) ? value.split(',').map(e => e.trim()) : value
      if (/Decimales/.test(current[0])) {
        previous[current[0]] = previous[current[0]].map(e => e ? parseFloat(e) : 0)
        previous['latitud'] = previous[current[0]][0]
        previous['longitud'] = previous[current[0]][1]
      }
    }
    return previous
  }, {})
}

exports.getCentroMunicipalData = number => parseInt(number) ? getCentroMunicipalObj(number) : number.split('').filter(el => parseInt(el)).find(getCentroMunicipalObj)

exports.sumValueToLatLon = geoReferencia => {
  const key = 'Decimales'
  const geoRef = {}
  geoRef[key] = geoReferencia[key].map(e => e + 0.0001)
  geoRef['latitud'] = geoRef[key][0]
  geoRef['longitud'] = geoRef[key][1]
  return geoRef
}

/* FIN - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES GENERAL */

/* INICIO - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES 2018 */

let refugioNameCoincidenceNumberObj = {}

let refugioNameforSaveIfIsThereExistingObj = {}

let refugioNameMatchExactList = []

const titleMapper2018 = {
  'Nº': '',
  'BAÑADO': 'zona',
  'CENTRO MUNICIPAL': 'centroMunicipal',
  'BARRIO': 'barrio',
  'COORDINADOR / A': 'coordinadores',
  'TELEFONO': '',
  'N° R.': '',
  'NOMBRE DEL REFUGIO': 'nombre',
  'CALLE Y O REFERENCIA': 'direccion',
  'CANTIDAD DE FAMILIAS': 'cantidadFamilias',
  'CANTIDAD DE FAMILIAS ACTUAL': '',
  'COORDINADORES': 'coordinadores',
  'CELULARES': '',
  'LIMPIEZA DE BAÑOS A CARGO DGRRD - TOTAL DE BAÑOS': '',
  'CANTIDAD DE CAMIONES': 'cantidadCamiones',
  'ESTADO DEL ALBERGUE': 'estado'
}

const _clearStringListOfAcentos = str => str.map(s => s.replace(/'/g, ' ')).map(s => s.replace(/`/g, ' ')).map(s => s.replace(/´/g, ' '))

const _clearStringListOfSpecialChar = strlist => strlist.map(s => s.replace(/\./g, '')).map(s => s.replace(/\(/g, '')).map(s => s.replace(/\)/g, '')).map(s => s.replace(/-/g, '')).map(s => s.trim()).filter(s => !!s)

const _getCompareList = pattern => _clearStringListOfSpecialChar(_clearStringListOfAcentos(pattern.toString().trim().toUpperCase().split(' ')))

const _hasRefugioNameCoincidenceNumberObj = (name, key) => refugioNameCoincidenceNumberObj[name] && refugioNameCoincidenceNumberObj[name][key]

const _canSumRefugioNameCoincidenceNumberObj = (name, key, timesItMatches) => !_hasRefugioNameCoincidenceNumberObj(name, key) || timesItMatches > refugioNameCoincidenceNumberObj[name][key]

const _verifyIfCanAndSumRefugioNameCoincidenceNumberObj = (name, key, timesItMatches, addValue) => {
  if (_canSumRefugioNameCoincidenceNumberObj(name, key, timesItMatches)) {
    if (!refugioNameCoincidenceNumberObj[name]) refugioNameCoincidenceNumberObj[name] = {}
    if (!refugioNameCoincidenceNumberObj[name][key]) refugioNameCoincidenceNumberObj[name][key] = 0
    if (addValue) {
      refugioNameCoincidenceNumberObj[name][key] += addValue
    } else {
      refugioNameCoincidenceNumberObj[name][key]++
    }
  }
}

const _compareRefugioNameByKeySplitedList = (comparelist1, comparelist2, name, key) => {
  let timesItMatches = 0
  comparelist1.forEach(val1 => {
    comparelist2.forEach(val2 => {
      if (val1 === val2) {
        timesItMatches++
        _verifyIfCanAndSumRefugioNameCoincidenceNumberObj(name, key, timesItMatches)
      }
    })
  })
}

const _compareRefugioNameByKey = (comparelist1, name, key) => {
  if (refugioNameMatchExactList.find(n => n === name)) return
  if (refugioNameMatchExactList.find(n => n === key)) return
  const result = key.trim().toUpperCase() === name.toString().trim().toUpperCase()
  if (result) {
    const timesItMatches = _hasRefugioNameCoincidenceNumberObj(name, key) || 1
    _verifyIfCanAndSumRefugioNameCoincidenceNumberObj(name, key, timesItMatches, 100)
    refugioNameMatchExactList.push(name)
    return
  }
  _compareRefugioNameByKeySplitedList(comparelist1, _getCompareList(key), name, key)
}

const _compareNameWithAllKeyInRefugioObj = (refugioObj, name) => {
  const exactName = refugioNameMatchExactList.find(n => n === name)
  if (exactName) return exactName
  Object.keys(refugioObj).forEach(key => _compareRefugioNameByKey(_getCompareList(name), name, key))
  const coincidenceObj = refugioNameCoincidenceNumberObj[name]
  if (coincidenceObj) {
    let refugioNameCoincidenceNumberObjKeys = Object.keys(coincidenceObj)
    if (refugioNameCoincidenceNumberObjKeys.length === 1) {
      return refugioNameCoincidenceNumberObjKeys[0]
    } else if (refugioNameCoincidenceNumberObjKeys.length > 1) {
      const maxValue = Math.max.apply(Math, refugioNameCoincidenceNumberObjKeys.map(key => coincidenceObj[key]))
      refugioNameCoincidenceNumberObjKeys.forEach(key => {
        if (coincidenceObj[key] && coincidenceObj[key] !== maxValue) {
          delete coincidenceObj[key]
        }
      })
      refugioNameCoincidenceNumberObjKeys = Object.keys(coincidenceObj)
      if (refugioNameCoincidenceNumberObjKeys.length === 1) {
        return refugioNameCoincidenceNumberObjKeys[0]
      }
    }
  }
  return null // No encontró el nombre del refugio dentro del objeto refugioObj
}

const _isThereExistingRefugioName = (refugioObj, name) => _compareNameWithAllKeyInRefugioObj(refugioObj, name)

exports.clearCordinadores = cordinadoresList => JSON.parse(JSON.stringify(cordinadoresList)).filter(c => (c.nombre || c.telefono))

exports.getRefugios2018Title = str => str && str.length ? titleMapper2018[str] ? titleMapper2018[str] : str : ''

exports.isThereExistingRefugio = _isThereExistingRefugioName

exports.getRefugioNameInObjectOrParams = (refugioObject, name) => {
  const refugioObj = _clone(refugioObject)
  const refugioName = _isThereExistingRefugioName(refugioObj, name)
  if (refugioName) {
    if (refugioNameforSaveIfIsThereExistingObj[refugioName]) {
      return refugioNameforSaveIfIsThereExistingObj[refugioName]
    } else {
      refugioNameforSaveIfIsThereExistingObj[refugioName] = refugioName
      return refugioName
    }
  } else {
    return name
  }
}

exports.saveRefugios2018File = (refugios, path) => _saveFile(refugios, `${path.replace('.json', '')}-2018.json`)

/* FIN - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES 2018 */

/* FIN - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES ESTADÍSTICAS */

const titleMapperStatistic = {
  'ORDEN': '',
  'Ubicado En Refugio': 'nombre',
  'NOMBRE Y APELLIDOS': 'responsable',
  'C.I.Nro.': 'cedulaDeIdentidad',
  'SALE DE de la Comunidad(Barrio)': '',
  'Zona': '',
  'Celular': '',
  'ADULTOS': 'adultos',
  'NIÑAS': 'ninhias',
  'NI�AS': 'ninhias',
  'NIÑOS': 'ninhios',
  'NI�OS': 'ninhios',
  'Adulto Mayor': 'adultosMayores',
  'Discapasitados': 'discapacitados',
  'Tipo de Vivienda(Propia - Alquilada - Compartida - Ocupada - Prestada)': 'tipoDeVivienda',
  'CHAPA': 'chapa',
  'TERCIADA': 'terciada',
  'PUNTAL': 'puntal',
  'OTROS': 'otros',
  'Referencia': '',
  'ASISTIDO POR': '',
  'Cantidad': ''
}

exports.getRefugiosStatisticTitle = str => str && str.length ? titleMapperStatistic[str] ? titleMapperStatistic[str] : str : ''

exports.saveStatisticRefugiosFile = (refugios, path) => _saveFile(refugios, `${path.replace('.json', '')}-statistic.json`)

exports.printRefugioNameCoincidenceNumberObj = () => {
  console.log(chalk.green('\nINICIA OBJETO MATCH de REFUGIO NOMBRE\n'))
  console.log(chalk.rgb(102, 217, 239)(JSON.stringify(refugioNameCoincidenceNumberObj, null, 2)))
  console.log(chalk.green('\nFINALIZA OBJETO MATCH de REFUGIO NOMBRE\n'))
}
/* FIN - FUNCIONALIDADES PARA GESTIÓN DE INUNDACIONES ESTADÍSTICAS */
