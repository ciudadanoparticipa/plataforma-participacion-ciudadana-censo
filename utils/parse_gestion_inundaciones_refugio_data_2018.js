#!/bin/node

const path = require('path')
const chalk = require('chalk')
const CsvReadableStream = require('csv-reader')
const {
  clone,
  clearCordinadores,
  getFileToJSON,
  getCSVFile,
  getRefugios2018Title,
  getRefugioNameInObjectOrParams,
  saveRefugios2018File,
  printRefugioNameCoincidenceNumberObj
} = require('./_common')

const srcPath = path.join(__dirname, '..', 'src')
const inputStreamPath = process.argv[2]
const refugioFilePath = process.argv[3] || path.join(srcPath, 'assets', 'json', 'refugios.json')

if (!inputStreamPath || !refugioFilePath) {
  console.error(chalk.red('Se necesita 2 argumentos, el path del csv con los datos estadísticos y el path del archivo json donde serán cargados estos datos'))
  process.exit(1)
}

const refugios = getFileToJSON(refugioFilePath)
const inputStream = getCSVFile(inputStreamPath)

const rowTitlesPosition = 4
const columnLimit = 14
const titles = []
const listaDeRefugiosSinDatosGeograficos = []

let currentGroup = ''
let refugiosToSave = clone(refugios)
let lastTelefono = ''

let rowIdx = 0
let colRefugioNamePosition = 7
let colCantFamiliasPosition = 9
let colCordinadorNombrePosition = 11
let colCordinadorTelPosition = 12
let colEstadoPosition = 15
let elements

inputStream
  .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
  .on('data', row => {
    rowIdx++
    elements = row.length
    if (!elements || elements < columnLimit) return
    elements = row.filter(e => !!e).length
    if (rowIdx === rowTitlesPosition) {
      row.forEach((column, idx) => {
        currentGroup = column.trim()
        if (currentGroup) {
          titles[idx] = getRefugios2018Title(currentGroup)
        }
      })
    } else if (row[0] !== 'Nº' && row[colRefugioNamePosition] && elements > 9) {
      const nombreRefugio = getRefugioNameInObjectOrParams(refugios, row[colRefugioNamePosition])
      if (refugios[nombreRefugio]) {
        refugiosToSave[nombreRefugio] = {
          ...refugios[nombreRefugio],
          'cantidadFamilias': row[colCantFamiliasPosition],
          'estado': row[colEstadoPosition] === 'NO ACTIVO' ? 'INACTIVO' : row[colEstadoPosition],
          'coordinadores': clearCordinadores(refugios[nombreRefugio]['coordinadores'])
        }
        const telefono = row[colCordinadorTelPosition] === 'll' ? lastTelefono : row[colCordinadorTelPosition]
        lastTelefono = telefono
        refugiosToSave[nombreRefugio]['coordinadores'].push({
          nombre: row[colCordinadorNombrePosition],
          telefono
        })
      } else {
        listaDeRefugiosSinDatosGeograficos.push(row[colRefugioNamePosition])
      }
    }
  })
  .on('end', () => {
    printRefugioNameCoincidenceNumberObj()
    console.log('\nLista de refugios que no poseen información de geolocalización: \n', listaDeRefugiosSinDatosGeograficos)
    console.log('\n\nLista de refugios sin información sobre su ESTADO: \n', Object.values(refugiosToSave).filter(r => !r['estado']).map(r => r.nombre))
    Object.keys(refugiosToSave).filter(key => !refugiosToSave[key]['estado']).forEach(key => {
      refugiosToSave[key]['estado'] = 'INACTIVO'
    })
    saveRefugios2018File(refugiosToSave, refugioFilePath)
  })
