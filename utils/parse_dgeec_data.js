#!/bin/node

const chalk = require('chalk')
const CsvReadableStream = require('csv-reader')
const { getFileToJSON, getCSVFile, saveFile, formatNumber, adaptDataToBaseProjects, getUsefulDataWithSubProject } = require('./_common')

const log = console.log
const warn = console.warn
const inputStreamPath = process.argv[2]
const baseProjectsPath = process.argv[3]
let baseProjects
let inputStream

if (!inputStreamPath || !baseProjectsPath) {
  console.error(chalk.red('Se necesita 2 argumentos, el path del csv con los datos estadísticos y el path del archivo json donde serán cargados estos datos'))
  process.exit(1)
}

baseProjects = getFileToJSON(baseProjectsPath)
inputStream = getCSVFile(inputStreamPath)

const statistics = {}
let currentGroup = ''

const rowLimit = 130
const rowThatContainProjectNameNumber = 4
const rowThatContainSubProjectNameNumber = 5

let rowIdx = 0
let projectName
let projectNumber
let subProjectIndex = 2
let cantOfSubprojects = 0
let elements, auxSplit

inputStream
  .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
  .on('data', row => {
    rowIdx++
    elements = row.filter(element => element).length
    if (!elements || rowIdx >= rowLimit) return
    if (rowThatContainProjectNameNumber === rowIdx) {
      row.forEach(element => {
        if (!element) {
          if (projectName) cantOfSubprojects++
          return
        }
        // Se carga la cantidad de subproyectos del proyecto del anterior ciclo
        if (cantOfSubprojects && projectName) {
          statistics[projectName]['cantOfSubprojects'] = cantOfSubprojects
        }
        // Se obtiene el nombre del proyecto y se guarda como key en el objeto estadísticas
        cantOfSubprojects = 1
        auxSplit = element.split(') ')
        projectNumber = auxSplit[0].trim()
        projectName = auxSplit[1].trim()
        statistics[projectName] = { projectNumber, cantOfSubprojects, subProjects: {} }
      })
      return
    }
    if (elements === 1) {
      currentGroup = row[0].trim()
      if (currentGroup) {
        Object.keys(statistics).forEach(projName => {
          Object.keys(statistics[projName]['subProjects']).forEach(subProjName => {
            statistics[projName]['subProjects'][subProjName][currentGroup] = {}
          })
        })
      }
    } else {
      if (rowThatContainSubProjectNameNumber === rowIdx) {
        Object.keys(statistics).forEach(projName => {
          for (let i = 0; i < statistics[projName].cantOfSubprojects; i++) {
            const subProjectName = row[subProjectIndex].split('(')[0].trim()
            statistics[projName]['subProjects'][subProjectName] = {}
            subProjectIndex++
          }
        })
      } else {
        const key = row[0].trim()
        if (key) {
          subProjectIndex = 2
          Object.keys(statistics).forEach(projName => {
            Object.keys(statistics[projName]['subProjects']).forEach(subProjName => {
              statistics[projName]['subProjects'][subProjName][currentGroup][key] = formatNumber(row[subProjectIndex])
              subProjectIndex++
            })
          })
        }
      }
    }
  })
  .on('end', () => {
    let allStatisticData = Object.assign({}, adaptDataToBaseProjects(statistics))
    baseProjects.forEach(project => {
      const estadisticas = getUsefulDataWithSubProject(allStatisticData[project.name])
      if (estadisticas) {
        delete allStatisticData[project.name]
      } else {
        log(chalk.red(`No hay datos estadísticos para el proyecto con el nombre: ${project.name}`))
      }
      project.estadisticas = estadisticas
    })

    Object.keys(allStatisticData).forEach(projectName => {
      warn(chalk.yellow(`Proyectos sin referencia o sub-proyectos: ${projectName}`))
    })

    saveFile(baseProjects, baseProjectsPath)
  })
