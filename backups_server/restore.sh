#!/bin/bash

echo "Start restore"

BACKUP_DB_NAME=backup.sql
BACKUP_ZIP_NAME=latest.zip
DATABASE_NAME=redmine
DOCKER_API_STATIC_DIR=/usr/src/app/public/

# Cambiar unicamente estas 3 variables:
BACKUP_SERVER_IP=51.15.15.75
DOCKER_DB_CONTAINER_NAME=mapa-asu-vuejs_db_1
DOCKER_API_CONTAINER_NAME=mapa-asu-vuejs_api_1

# NO CAMBIAR estas variables:
BACKUP_SERVER_URL=${BACKUP_SERVER_IP}:12004
BACKUP_DB_URL=${BACKUP_SERVER_URL}/db/$BACKUP_ZIP_NAME
BACKUP_API_URL=${BACKUP_SERVER_URL}/api/$BACKUP_ZIP_NAME

docker-compose up -d db

echo "Start restor db"
echo "curl -LO $BACKUP_DB_URL"
curl -LO $BACKUP_DB_URL

echo "unzip $BACKUP_ZIP_NAME"
unzip $BACKUP_ZIP_NAME
rm $BACKUP_ZIP_NAME

echo "Restor in docker"
echo "cat $BACKUP_DB_NAME cat | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME"
cat $BACKUP_DB_NAME cat | docker exec -i $DOCKER_DB_CONTAINER_NAME psql -U $DATABASE_NAME
rm $BACKUP_DB_NAME

echo "Finished restore db"

docker-compose up -d api

echo "Start restor api"
echo "curl -LO $BACKUP_API_URL"
curl -LO $BACKUP_API_URL

echo "unzip $BACKUP_ZIP_NAME"
unzip $BACKUP_ZIP_NAME
rm $BACKUP_ZIP_NAME

echo "Copy static api folder in docker container"
docker exec -t $DOCKER_API_CONTAINER_NAME mkdir -p public
echo "docker cp ./api_static_folder/. $DOCKER_API_CONTAINER_NAME:$DOCKER_API_STATIC_DIR/. "
docker cp ./api_static_folder/. $DOCKER_API_CONTAINER_NAME:$DOCKER_API_STATIC_DIR/.
rm -rf ./api_static_folder

docker-compose up -d

cat README.md
