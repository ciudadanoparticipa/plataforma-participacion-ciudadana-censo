FROM nginx:1.15-alpine

LABEL maintainer Arturo Volpe

RUN apk add --update nodejs nodejs-npm jpegoptim optipng && npm install minify-json -g

COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

COPY ./dist /usr/share/nginx/html
