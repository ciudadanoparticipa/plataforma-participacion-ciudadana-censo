const url = process.env.SERVER_URL || 'http://asuparticipa.reclamos.asuncion.gov.py/' // 'http://localhost:3000'
const statusId = process.env.STATUS_ID || 1

module.exports = {
  redmine: {
    url,
    statusId,
    customFields: {
      NOMBRE: process.env.CUSTOMFIELD_NOMBRE || 'Nombre',
      EMAIL: process.env.CUSTOMFIELD_EMAIL || 'Email',
      LATITUD: process.env.CUSTOMFIELD_LATITUD || 'Latitud',
      LONGITUD: process.env.CUSTOMFIELD_LONGITUD || 'Longitud',
      TIPO_RECLAMO: process.env.CUSTOMFIELD_TIPO_RECLAMO || 'Tipo de Reclamo',
      ISSUE_IDENTIFIER: process.env.CUSTOMFIELD_IDENTIFICADOR || 'Identificador del Mensaje',
      PROJECTINFO: process.env.PROJECTINFO || 'projectInfo',
      AUDITORIA: process.env.CUSTOMFIELD_IDENTIFICADOR || 'auditoria'
    },
    roles: {
      PROJECT_EDITOR: process.env.PROJECT_EDITOR || 'Gestor de contenido'
    }
  }
}
