const chalk = require('chalk')
const { formatDate, getDefaultOptions, getUrl } = require('../utils')

module.exports = class RedmineApi {
  constructor (config, request) {
    this._cache = {}
    this.config = config
    this.request = request
    this.setInitialValues()
  }

  setInitialValues () {
    this.setInitialOptions({ apiKey: this.config.apiKey })
  }

  setInitialOptions (options) {
    this.options = getDefaultOptions(options)
  }

  _getUrl (resource, opts = {}, addToken = { addToken: true }) {
    return getUrl(this.config, resource, { ...this.options, ...opts }, addToken)
  }

  _api (options) {
    return this.request(options)
  }

  _onError (err) {
    console.error(chalk.red(`\n[${formatDate()}] Error al informacion de Redmine`, err.toString()), err)
  }
}
