const chalk = require('chalk')
const HttpStatus = require('http-status-codes')
const RedmineApi = require('./redmine-api')
const { uniqBy } = require('lodash/array')
const { formatDate, getDefaultOptions, clone, HttpError } = require('../utils')

const getRolesByUser = user => {
  if (user && user.memberships) {
    return uniqBy(user.memberships.reduce((previousValue, currentValue) => {
      if (currentValue && currentValue.roles && currentValue.roles.length) previousValue.push(...uniqBy(currentValue.roles, 'id'))
      return previousValue
    }, []), 'id')
  }
  throw new HttpError(`El usuario ${user.login} no tiene roles`, HttpStatus.UNAUTHORIZED)
}

const findRoleByName = (roles, roleName) => roles.find(role => role.name === roleName)

module.exports = class LoggedRedmineApi extends RedmineApi {
  signIn ({ username, password }) {
    return this._api(this._getUrl('users', { query: `name=${username}` }))
      .then(data => data.users)
      .then(users => {
        if (users && users.length) return users[0]
        throw new HttpError(`El usuario ${username} no existe o ha sido bloqueado`, HttpStatus.NOT_FOUND)
      })
      .then(user => {
        const { url } = this.config
        const uri = `http://${username.split('@')[0]}:${password}@${url.split('://')[1]}/users/${user.id}.json?include=memberships,groups`
        return this._api(getDefaultOptions({ uri }, false))
      })
      .then(response => this._validateUserPermissions(response))
      .then(response => {
        console.log(chalk.green(`\n[El usuario "${username}" se ha logueado el ${formatDate()}]\n`))
        return response
      })
  }

  _validateUserPermissions (response) {
    const user = clone(response.user)
    const roles = getRolesByUser(user)
    if (user.is_admin || findRoleByName(roles, this.config.roles.PROJECT_EDITOR)) {
      delete user.groups
      delete user.memberships
      delete user.is_admin
      response.user = clone(user)
      return response
    }
    throw new HttpError(`El usuario ${user.login} no tiene permiso para ingresar`, HttpStatus.FORBIDDEN)
  }
}
